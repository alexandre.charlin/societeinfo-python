## 1.0.0

**Major:**

- Manage "page" parameter for autocomplete companies call (this implies a signature change for autocomplete_companies).

## 0.1.0

**Minor:**

- First version, including calls to get credits details, autocomplete companies, and get a company by its identifier.
